FROM node:15

# Create app directory
WORKDIR /user/src/app

# Install app dependencies
# Use wildcard to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 5003
CMD [ "node", "server.js" ]