const express = require("express");
const {
  getProject,
  createProject, 
  getProjects,
  updateProject,
  deleteProject,
} = require("../controllers/projects");
const Project = require("../models/Project");

// Include other resource routers TODO: replace comments with fileFolders
const fileFolderRouter = require("./filefolders");

const router = express.Router({
  mergeParams: true,
});
const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

// Re-route into other resource routers TODO: Replace with fileFolders route and router
router.use("/:projectId/filefolders", fileFolderRouter);

router
  .route("/")
  .get(advancedResults(Project, {
    path: "filefolders",
    select: "name type",
  }), getProjects)
  .post(protect, authorize("user", "moderator", "admin"), createProject);

router
  .route("/:id")
  .get(getProject)
  .put(protect, authorize("user", "moderator", "admin"), updateProject)
  .delete(protect, authorize("user", "moderator", "admin"), deleteProject);

module.exports = router;
