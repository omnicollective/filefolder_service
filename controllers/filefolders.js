const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Comment = require("../models/Comment");
const User = require("../models/User");
const Post = require("../models/Post");
const Project = require("../models/Project");
const FileFolder = require("../models/FileFolder");

// @desc    Get all filefolders for a project
// @route   GET /api/v1/filefolders
// @route   GET /api/v1/projects/:projectId/filefolders
// @access  Public
exports.getFileFolders = asyncHandler(async (req, res, next) => {
  if (req.params.projectId) {
    const filefolders = await FileFolder.find({ project: req.params.projectId });
    return res.status(200).json({
      success: true,
      count: filefolders.length,
      data: filefolders,
    });
  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc    Get single filefolder
// @route   GET /api/v1/filefolders/:id
// @access  Public
exports.getFileFolder = asyncHandler(async (req, res, next) => {
  const filefolder = await FileFolder.findById(req.params.id).populate({
    path: "project",
    select: "name text",
  });

  if (!filefolder) {
    return next(
      new ErrorResponse(`No item with the id of ${req.params.id}`, 404)
    );
  }

  res.status(200).json({
    success: true,
    data: filefolder,
  });
});

// @desc    Create filefolder on project
// @route   POST /api/v1/projects/:projectId/filefolders
// @access  Private
exports.createFileFolder = asyncHandler(async (req, res, next) => {
  req.body.project = req.params.projectId;
  req.body.user = req.user.id;

  const project = await Project.findById(req.params.projectId);

  if (!project) {
    return next(
      new ErrorResponse(`No project with the id of ${req.params.projectId}`, 404)
    );
  }

  const filefolder = await FileFolder.create(req.body);

  res.status(200).json({
    success: true,
    data: filefolder,
  });
});

// @desc    Update filefolder
// @route   PUT /api/v1/filefolders/:id
// @access  Private
exports.updateFileFolder = asyncHandler(async (req, res, next) => {
  let filefolder = await FileFolder.findById(req.params.id);

  if (!filefolder) {
    return next(
      new ErrorResponse(`No item with the id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is  owner
  if (filefolder.user.toString() !== req.user.id) {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to update item ${filefolder._id}`,
        401
      )
    );
  }

  filefolder = await FileFolder.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: filefolder,
  });
});

// @desc    Delete filefolder
// @route   DELETE /api/v1/filefolders/:id
// @access  Private
exports.deleteFileFolder = asyncHandler(async (req, res, next) => {
  const filefolder = await FileFolder.findById(req.params.id);

  if (!filefolder) {
    return next(
      new ErrorResponse(`No item with the id of ${req.params.id}`, 404)
    );
  }

  if (filefolder.user.toString() !== req.user.id) {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to delete item, ${filefolder._id}`,
        401
      )
    );
  }

  await filefolder.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});
