const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Collective = require("../models/Collective");
const User = require("../models/User");
const Post = require("../models/Post");
const Project = require("../models/Project");

// @desc    Get all projects for a post
// @route   GET /api/v1/projects
// @route   GET /api/v1/posts/:postId/projects
// @access  Public
exports.getProjects = asyncHandler(async (req, res, next) => {
  if (req.params.postId) {
    const projects = await Project.find({ post: req.params.postId });
    let projects2 = JSON.parse(JSON.stringify({data: projects}));
    return res.status(200).json({
      success: true,
      count: projects.length,
      data: projects2,
      });
  } else {
    let projects2 = JSON.parse(JSON.stringify(res.advancedResults));
    for (let i = 0; i < projects2.data.length; i++){
      const user = await User.findById(projects2.data[i].user);
      let username = JSON.parse(JSON.stringify({data: user})).data.username;
      let addUN  = {
        username: username ? username : "ERROR FINDING USER",
      }
      projects2.data[i] = {...projects2.data[i], ...addUN};
    } 
    res.status(200).json(projects2);
  }
});

// @desc    Get single project
// @route   GET /api/v1/projects/:id
// @access  Public
exports.getProject = asyncHandler(async (req, res, next) => {
  const project = await Project.findById(req.params.id).populate({
    path: "post",
    select: "title text",
  });

  if (!project) {
    return next(
      new ErrorResponse(`No project with the id of ${req.params.id}`, 404)
    );
  }

  res.status(200).json({
    success: true,
    data: project,
  });
});

// @desc    Create new project
// @route   POST /api/v1/projects
// @route   POST /api/v1/posts/:postId/projects

// @access  Private
exports.createProject = asyncHandler(async (req, res, next) => {
  console.log(req.body);
  console.log(req.params);
  req.body.user = req.user.id;
  if (req.body.withPost === true && req.params.postId !== null) {
    req.body.post = req.params.postId;
    const post = await Post.findById(req.params.postId);
    if (!post) {
      return next(
        new ErrorResponse(
          `No post with the id of ${req.params.postId}`,
          404
        )
      );
    }
  }
  const project = await Project.create(req.body);
  res.status(200).json({
    success: true,
    data: project,
  });
});

// @desc    Update project
// @route   PUT /api/v1/projects/:id
// @access  Private
exports.updateProject = asyncHandler(async (req, res, next) => {
  let project = await Project.findById(req.params.id);

  if (!project) {
    return next(
      new ErrorResponse(`No project with the id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is post owner
  if (project.user.toString() !== req.user.id) {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to update project ${project._id}`,
        401
      )
    );
  }

  project = await Project.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: project,
  });
});

// @desc    Delete project
// @route   DELETE /api/v1/project/:id
// @access  Private
exports.deleteProject = asyncHandler(async (req, res, next) => {
  const project = await Project.findById(req.params.id);

  if (!project) {
    return next(
      new ErrorResponse(`No project with the id of ${req.params.id}`, 404)
    );
  }

  if (project.user.toString() !== req.user.id) {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to delete project ${project._id}`,
        401
      )
    );
  }

  await project.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});
