const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const FileFolder = require("../models/FileFolder");

// @desc    Get all files for a filefolder
// @route   GET /api/v1/filefolders
// @route   GET /api/v1/projects/:projectId/filefolders/:filefolderId/files
// @access  Public
exports.getFiles = asyncHandler(async (req, res, next) => {
    if (req.params.parentId) {
        const files = await FileFolder.find({ parentId: req.params.parentId });
        return res.status(200).json({
            success: true,
            count: files.length,
            data: files
        });
    } else {
        console.log(`not working`);
        res.status(200).json(res.advancedResults);
    }
});

// @desc    Create file on filefolder
// @route   POST /api/v1/projects/:projectId/filefolders/:filefolderId/files
// @access  Private
exports.createFile = asyncHandler(async (req, res, next) => {
    req.body.project = req.params.projectId
    req.body.parentId = req.params.filefolderId;
    req.body.user = req.user.id;

    const filefolder = await FileFolder.findById(req.params.filefolderId);

    if (!filefolder) {
        return next(
            new ErrorResponse(`No item with the id of ${req.params.filefolderId}`, 404)
        );
    }

    const file = await FileFolder.create(req.body);

    res.status(200).json({
        success: true,
        data: file,
    });
});
