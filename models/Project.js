const mongoose = require("mongoose");
const slugify = require("slugify");
const Populate = require("../utils/autopopulate");

const ProjectSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Please add a title"],
    unique: false,
    trim: true,
    maxlength: [50, "Title cannot be more than 50 characters"],
  },
  withPost: {
    type: Boolean,
  },
  slug: String,
  description: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  post: {
    type: mongoose.Schema.ObjectId,
    ref: 'Post',
    required: false
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: true,
  },
},
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  });

// Reverse populate with virtuals for files
ProjectSchema.virtual('filefolders', {
  ref: 'FileFolder',
  localField: '_id',
  foreignField: 'project',
  justOne: false
});

// Get slug for project
ProjectSchema.pre("save", function (next) {
  const projectID = this._id;
  this.slug = slugify(projectID.toString());
  next();
});

// Cascade delete files/folders if a project is deleted
ProjectSchema.pre("remove", async function (next) {
  console.log(`files/folders being removed from project ${this._id}`);
  await this.model("FileFolder").deleteMany({
    project: this._id,
  });
  next();
});






module.exports = mongoose.model("Project", ProjectSchema);
