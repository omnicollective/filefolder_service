const mongoose = require("mongoose");
const Populate = require("../utils/autopopulate");

const FileFolderSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please enter a name"]
  },
  isFile: {
    type: Boolean,
  },
  type: {
    type: [String],
    enum: [
      "js", 
      "json",
      "html",
      "svelte",
      "go",
      "php",
      "py",
      "sh",
      "rb",
      "c",
      "cpp",
      "cs",
      "java"
    ],
    required: false
  },
  source: {
    type: String,
    required: false
  },
  slug: String,
  createdAt: {
    type: Date,
    default: Date.now
  },
  // replBody : Object,
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: true
  },
  project: {
    type: mongoose.Schema.ObjectId,
    ref: 'Project',
    required: true
  },
  parentId: {
    type: mongoose.Schema.ObjectId,
    ref: "FileFolder",
    required: false
  }
},
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  });

module.exports = mongoose.model("FileFolder", FileFolderSchema);
